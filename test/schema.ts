import * as tape from "tape";
import {validate} from "../src";
import type {Schema} from "../src";

tape('validate', ({test}) => {
    test('string', ({test}) => {
        test('type', ({same, end}) => {
            const schema: Schema<string> = {
                type: "string"
            };

            const errors = validate(1 as any, schema);

            same(errors.length, 1);
            same(errors[0].rawMessage, 'must be of type "string"');
            same(errors[0].path, undefined);

            end();
        });

        test('length', ({test}) => {
            test('fixed', ({same, end}) => {
                const schema: Schema<string> = {
                    type: "string",
                    length: 2
                };

                same(validate('12', schema).length, 0);

                const errors = validate('123', schema);

                same(errors.length, 1);
                same(errors[0].rawMessage, 'must have a length of 2');
                same(errors[0].path, undefined);

                end();
            });

            test('min', ({same, end}) => {
                const schema: Schema<string> = {
                    type: "string",
                    length: {
                        min: 2
                    }
                };

                same(validate('12', schema).length, 0);

                const errors = validate('1', schema);

                same(errors.length, 1);
                same(errors[0].rawMessage, 'must have a minimum length of 2');
                same(errors[0].path, undefined);

                end();
            });

            test('max', ({same, end}) => {
                const schema: Schema<string> = {
                    type: "string",
                    length: {
                        max: 1
                    }
                };

                same(validate('1', schema).length, 0);

                const errors = validate('12', schema);

                same(errors.length, 1);
                same(errors[0].rawMessage, 'must have a maximum length of 1');
                same(errors[0].path, undefined);

                end();
            });
        });

        test('value', ({test}) => {
            test('fixed', ({same, end}) => {
                const schema: Schema<string> = {
                    type: "string",
                    value: '1'
                };

                same(validate('1', schema).length, 0);

                const errors = validate('2', schema);

                same(errors.length, 1);
                same(errors[0].rawMessage, 'must be equal to "1"');
                same(errors[0].path, undefined);

                end();
            });

            test('min', ({same, end}) => {
                const schema: Schema<string> = {
                    type: "string",
                    value: {
                        min: '2'
                    }
                };

                same(validate('2', schema).length, 0);

                const errors = validate('1', schema);

                same(errors.length, 1);
                same(errors[0].rawMessage, 'must be greater than or equal to "2"');
                same(errors[0].path, undefined);

                end();
            });

            test('max', ({same, end}) => {
                const schema: Schema<string> = {
                    type: "string",
                    value: {
                        max: '1'
                    }
                };

                same(validate('1', schema).length, 0);

                const errors = validate('2', schema);

                same(errors.length, 1);
                same(errors[0].rawMessage, 'must be lower than or equal to "1"');
                same(errors[0].path, undefined);

                end();
            });
        });

        test('match', ({same, end}) => {
            const schema: Schema<string> = {
                type: "string",
                match: /a/
            };

            same(validate('a', schema).length, 0);

            const errors = validate('b', schema);

            same(errors.length, 1);
            same(errors[0].rawMessage, 'must match /a/');
            same(errors[0].path, undefined);

            end();
        });
    });

    test('number', ({test}) => {
        test('type', ({same, end}) => {
            const schema: Schema<number> = {
                type: "number"
            };

            same(validate(1, schema).length, 0);

            const errors = validate('1' as any, schema);

            same(errors.length, 1);
            same(errors[0].rawMessage, 'must be of type "number"');
            same(errors[0].path, undefined);

            end();
        });

        test('value', ({test}) => {
            test('fixed', ({same, end}) => {
                const schema: Schema<number> = {
                    type: "number",
                    value: 1
                };

                same(validate(1, schema).length, 0);

                const errors = validate(2, schema);

                same(errors.length, 1);
                same(errors[0].rawMessage, 'must be equal to 1');
                same(errors[0].path, undefined);

                end();
            });

            test('min', ({same, end}) => {
                const schema: Schema<number> = {
                    type: "number",
                    value: {
                        min: 2
                    }
                };

                same(validate(2, schema).length, 0);

                const errors = validate(1, schema);

                same(errors.length, 1);
                same(errors[0].rawMessage, 'must be greater than or equal to 2');
                same(errors[0].path, undefined);

                end();
            });

            test('max', ({same, end}) => {
                const schema: Schema<number> = {
                    type: "number",
                    value: {
                        max: 1
                    }
                };

                same(validate(1, schema).length, 0);

                const errors = validate(2, schema);

                same(errors.length, 1);
                same(errors[0].rawMessage, 'must be lower than or equal to 1');
                same(errors[0].path, undefined);

                end();
            });
        });
    });

    test('array', ({test}) => {
        test('type', ({same, end}) => {
            const schema: Schema<Array<string>> = {
                type: "array"
            };

            same(validate(['1'], schema).length, 0);

            const errors = validate(1 as any, schema);

            same(errors.length, 1);
            same(errors[0].rawMessage, 'must be of type "array"');
            same(errors[0].path, undefined);

            end();
        });

        test('length', ({test}) => {
            test('fixed', ({same, end}) => {
                const schema: Schema<Array<string>> = {
                    type: "array",
                    length: 2
                };

                same(validate(['1', '2'], schema).length, 0);

                const errors = validate(['1'], schema);

                same(errors.length, 1);
                same(errors[0].rawMessage, 'must have 2 elements');
                same(errors[0].path, undefined);

                end();
            });

            test('min', ({same, end}) => {
                const schema: Schema<Array<string>> = {
                    type: "array",
                    length: {
                        min: 2
                    }
                };

                same(validate(['1', '2'], schema).length, 0);

                const errors = validate(['1'], schema);

                same(errors.length, 1);
                same(errors[0].rawMessage, 'must have a minimum of 2 elements');
                same(errors[0].path, undefined);

                end();
            });

            test('max', ({same, end}) => {
                const schema: Schema<Array<string>> = {
                    type: "array",
                    length: {
                        max: 1
                    }
                };

                same(validate(['1'], schema).length, 0);

                const errors = validate(['1', '2'], schema);

                same(errors.length, 1);
                same(errors[0].rawMessage, 'must have a maximum of 1 elements');
                same(errors[0].path, undefined);

                end();
            });
        });

        test('each', ({same, end}) => {
            const schema: Schema<Array<string>> = {
                type: "array",
                each: {
                    type: "string",
                    value: '1'
                }
            };

            same(validate(['1'], schema).length, 0);

            const errors = validate(['2', '3'], schema);

            same(errors.length, 2);
            same(errors[0].rawMessage, 'must be equal to "1"');
            same(errors[0].path, '[0]');
            same(errors[1].rawMessage, 'must be equal to "1"');
            same(errors[1].path, '[1]');

            end();
        });

        test('elements', ({same, end}) => {
            const schema: Schema<Array<string | number>> = {
                type: "array",
                elements: [{
                    type: "string"
                }, {
                    type: "number"
                }]
            };

            same(validate(['1', 1], schema).length, 0);

            const errors = validate([2, '3'], schema);

            same(errors.length, 2);
            same(errors[0].rawMessage, 'must be of type "string"');
            same(errors[0].path, '[0]');
            same(errors[1].rawMessage, 'must be of type "number"');
            same(errors[1].path, '[1]');

            end();
        });
    });

    test('object', ({test}) => {
        type Candidate = {
            foo: string,
            bar: {
                foo: string
            }
        };

        test('type', ({same, end}) => {
            const schema: Schema<Candidate> = {
                type: "object"
            };

            same(validate({
                foo: '1',
                bar: {
                    foo: '1'
                }
            }, schema).length, 0);

            const errors = validate(1 as any, schema);

            same(errors.length, 1);
            same(errors[0].rawMessage, 'must be of type "object"');
            same(errors[0].path, undefined);

            end();
        });

        test('properties', ({test}) => {
            test('required', ({same, end}) => {
                const schema: Schema<Candidate> = {
                    type: "object",
                    properties: {
                        foo: {
                            type: "string",
                            required: true
                        },
                        bar: {
                            type: "object"
                        }
                    }
                };

                same(validate({
                    foo: '1',
                } as any, schema).length, 0);

                const errors = validate({} as any, schema);

                same(errors.length, 1);
                same(errors[0].rawMessage, 'is required');
                same(errors[0].path, 'foo');

                end();
            });

            test('sub-properties', ({same, end}) => {
                const schema: Schema<{
                    foo: string,
                    bar: {
                        foo: string,
                        bar: Array<string>
                    }
                }> = {
                    type: "object",
                    properties: {
                        foo: {
                            type: "string",
                            value: '1'
                        },
                        bar: {
                            type: "object",
                            properties: {
                                foo: {
                                    type: "string",
                                    value: '1'
                                },
                                bar: {
                                    type: "array",
                                    each: {
                                        type: "string"
                                    }
                                }
                            }
                        }
                    }
                };

                same(validate({
                    foo: '1',
                    bar: {
                        foo: '1',
                        bar: ['1']
                    }
                }, schema).length, 0);

                const errors = validate({
                    foo: 1 as any,
                    bar: {
                        foo: 1 as any,
                        bar: [1 as any]
                    }
                }, schema);

                same(errors.length, 3);
                same(errors[0].rawMessage, 'must be of type "string"');
                same(errors[0].path, 'foo');
                same(errors[1].rawMessage, 'must be of type "string"');
                same(errors[1].path, 'bar.foo');
                same(errors[2].rawMessage, 'must be of type "string"');
                same(errors[2].path, 'bar.bar[0]');

                end();
            });
        });
    });
});
