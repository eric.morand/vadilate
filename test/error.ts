import * as tape from "tape";

import {ValidationError} from '../src/lib/error';

tape('ValidationError', ({test}) => {
    test('constructor', ({same, end}) => {
        const error = new ValidationError('hello', 'foo');

        same(error.rawMessage, 'hello');
        same(error.path, 'foo');
        same(error.message, 'foo hello');

        end();
    });
});
