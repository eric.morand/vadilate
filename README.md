# Vadilate

```
Error: must be equal to "Validate"

░░░░░▄▄▄▄▀▀▀▀▀▀▀▀▄▄▄▄▄▄░░░░░░░
░░░░░█░░░░▒▒▒▒▒▒▒▒▒▒▒▒░░▀▀▄░░░░
░░░░█░░░▒▒▒▒▒▒░░░░░░░░▒▒▒░░█░░░
░░░█░░░░░░▄██▀▄▄░░░░░▄▄▄░░░░█░░
░▄▀▒▄▄▄▒░█▀▀▀▀▄▄█░░░██▄▄█░░░░█░
█░▒█▒▄░▀▄▄▄▀░░░░░░░░█░░░▒▒▒▒▒░█
█░▒█░█▀▄▄░░░░░█▀░░░░▀▄░░▄▀▀▀▄▒█
░█░▀▄░█▄░█▀▄▄░▀░▀▀░▄▄▀░░░░█░░█░
░░█░░░▀▄▀█▄▄░█▀▀▀▄▄▄▄▀▀█▀██░█░░
░░░█░░░░██░░▀█▄▄▄█▄▄█▄████░█░░░
░░░░█░░░░▀▀▄░█░░░█░█▀██████░█░░
░░░░░▀▄░░░░░▀▀▄▄▄█▄█▄█▄█▄▀░░█░░
░░░░░░░▀▄▄░▒▒▒▒░░░░░░░░░░▒░░░█░
░░░░░░░░░░▀▀▄▄░▒▒▒▒▒▒▒▒▒▒░░░░█░
░░░░░░░░░░░░░░▀▄▄▄▄▄▄▄▄▄▄▄▄▄█░░
```

Validate an object against a schema.

[![NPM version][npm-image]][npm-url] [![Coverage percentage][coveralls-image]][coveralls-url]

## Usage

```ts
import {validate} from 'vadilate'
import type {Schema} from 'vadilate'

const schema: Schema<{
    name: string,
    age: number
}> = {
    name: {
        type: "string"
    },
    age: {
        type: "number"
    }
}

const errors = validate({
    name: 'Jane',
    age: 36
}, schema);
```

## API

Read the [documentation](https://nightlycommit.gitlab.io/vadilate/) for more information.

## Contributing

-   Fork the main repository
-   Code
-   Implement tests using [tape](https://www.npmjs.com/package/tape)
-   Issue a pull request keeping in mind that all pull requests must reference an issue in the issue queue

## License

Apache-2.0 © [Eric MORAND](<>)

[npm-image]: https://badge.fury.io/js/vadilate.svg
[npm-url]: https://npmjs.org/package/vadilate
[coveralls-image]: https://coveralls.io/repos/gitlab/nightlycommit/vadilate/badge.svg?branch=master
[coveralls-url]: https://coveralls.io/gitlab/nightlycommit/vadilate
