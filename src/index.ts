import {ValidationError} from "./lib/error";
import type {ArraySchema, BooleanSchema, NumberSchema, ObjectSchema, Schema, StringSchema} from "./model/schema";

export type {Schema} from "./model/schema";

type SchemaTarget<S> = S extends ObjectSchema<infer T> ? T :
    (S extends StringSchema ? string :
        (S extends NumberSchema ? number :
            (S extends ArraySchema<infer T> ? Array<T> :
                (S extends BooleanSchema ? boolean :
                    never))));

export function validate<S extends Schema<any>>(value: SchemaTarget<S>, against: S): Array<ValidationError> {
    const errors: Array<ValidationError> = [];

    const validateValue = (value: any, against: Schema<any>, path?: string): void => {
        const join = (key: string, separator: string = '.'): string => {
            const parts: Array<string> = [
                key
            ];

            if (path !== undefined) {
                parts.unshift(path);
            }

            return parts.join(separator);
        };

        const addError = (message: string): void => {
            errors.push(new ValidationError(message, path));
        }

        const {required, type} = against;

        const validateString = (value: string, against: StringSchema): void => {
            const {value: ruleValue, length: ruleLength, match: ruleMatch} = against as StringSchema;

            if (ruleValue !== undefined) {
                if (typeof ruleValue === 'string') {
                    if (ruleValue != value) {
                        addError(`must be equal to "${ruleValue}"`);
                    }
                } else {
                    const {min, max} = ruleValue;

                    if (min !== undefined) {
                        if (value < min) {
                            addError(`must be greater than or equal to "${min}"`);
                        }
                    }

                    if (max !== undefined) {
                        if (value > max) {
                            addError(`must be lower than or equal to "${max}"`);
                        }
                    }
                }
            }

            if (ruleLength !== undefined) {
                if (typeof ruleLength === 'number') {
                    if (value.length !== ruleLength) {
                        addError(`must have a length of ${ruleLength}`);
                    }
                } else {
                    const {min, max} = ruleLength;

                    if ((min !== undefined) && (value.length < min)) {
                        addError(`must have a minimum length of ${min}`);
                    }

                    if ((max !== undefined) && (value.length > max)) {
                        addError(`must have a maximum length of ${max}`);
                    }
                }
            }

            if (ruleMatch !== undefined) {
                if (!ruleMatch.test(value)) {
                    addError(`must match ${ruleMatch}`);
                }
            }
        };

        const validateNumber = (value: number, against: NumberSchema): void => {
            const {value: ruleValue} = against;

            if (ruleValue !== undefined) {
                if (typeof ruleValue === 'number') {
                    if (ruleValue != value) {
                        addError(`must be equal to ${ruleValue}`);
                    }
                } else {
                    const {min, max} = ruleValue;

                    if (min !== undefined) {
                        if (value < min) {
                            addError(`must be greater than or equal to ${min}`);
                        }
                    }

                    if (max !== undefined) {
                        if (value > max) {
                            addError(`must be lower than or equal to ${max}`);
                        }
                    }
                }
            }
        };

        const validateArray = (value: Array<any>, against: ArraySchema<any>): void => {
            const {each, length, elements} = against as ArraySchema<any>;

            if (each !== undefined) {
                let index: number = 0;

                for (let item of value) {
                    validateValue(item, each, join(`[${index++}]`, ''));
                }
            }

            if (elements !== undefined) {
                let index: number = 0;

                for (let element of elements) {
                    const item = value[index];

                    validateValue(item, element, join(`[${index++}]`));
                }
            }

            if (length !== undefined) {
                if (typeof length === 'number') {
                    if (value.length !== length) {
                        addError(`must have ${length} elements`);
                    }
                } else {
                    const {min, max} = length;

                    if ((min !== undefined) && (value.length < min)) {
                        addError(`must have a minimum of ${min} elements`);
                    }

                    if ((max !== undefined) && (value.length > max)) {
                        addError(`must have a maximum of ${max} elements`);
                    }
                }
            }
        };

        const validateObject = (object: Record<string, any>, against: ObjectSchema<any>): void => {
            const {properties} = against;

            for (let propertyName in properties) {
                const schema = properties[propertyName] as Schema<any>;
                const value = object[propertyName];

                validateValue(value, schema, join(propertyName));
            }
        }

        if (value === undefined || value === null) {
            if (required === true) {
                addError(`is required`);
            }
        } else {
            if ((type === "array") && (!Array.isArray(value)) ||
                ((type !== "array") && (typeof value !== type))) {
                addError(`must be of type "${type}"`);
            } else {
                if (type === "string") {
                    validateString(value, against as StringSchema);
                }

                if (type === "number") {
                    validateNumber(value, against as NumberSchema);
                }

                if (type === "array") {
                    validateArray(value, against as ArraySchema<any>);
                }

                if (type === "object") {
                    validateObject(value, against as ObjectSchema<any>);
                }
            }
        }
    }

    validateValue(value, against);

    return errors;
}