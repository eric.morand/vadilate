export class ValidationError extends Error {
  public readonly path?: string;
  public readonly rawMessage: string;

  constructor(rawMessage: string, path?: string) {
    const parts: Array<string> = [
      rawMessage
    ];

    if (path) {
      parts.unshift(path);
    }

    super(parts.join(' '));

    this.path = path;
    this.rawMessage = rawMessage;
  }
}
