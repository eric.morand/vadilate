export type MinMax<T> = T | {
    max: T;
    min: T;
} | {
    max: T;
    min?: T;
} | {
    max?: T;
    min: T;
};