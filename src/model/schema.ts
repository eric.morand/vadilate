import {MinMax} from "./min-max";

type BaseSchema<T extends 'array' | 'boolean' | 'number' | 'object' | 'string'> = {
    type: T;
    required?: boolean;
}

export type StringSchema = BaseSchema<"string"> & {
    length?: MinMax<number>;
    value?: MinMax<string>;
    match?: RegExp;
};

export type NumberSchema = BaseSchema<"number"> & {
    value?: MinMax<number>;
};

export type ArraySchema<T> = BaseSchema<"array"> & {
    each?: Schema<any>;
    elements?: Array<Schema<any>>;
    length?: MinMax<number>;
};

export type BooleanSchema = BaseSchema<"boolean">;

export type ObjectSchema<T extends Record<string, any>> = BaseSchema<"object"> & {
    properties?: {
        [K in keyof T]?: Schema<T[K]>;
    }
};

export type Schema<T> = T extends string ? StringSchema :
    (T extends Number ? NumberSchema :
        (T extends Boolean ? BooleanSchema :
            (T extends Array<infer Item> ? ArraySchema<Item> :
                (T extends Record<string, any> ? ObjectSchema<T> :
                    never))));


